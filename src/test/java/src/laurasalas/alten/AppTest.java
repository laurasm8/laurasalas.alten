package src.laurasalas.alten;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import src.laurasalas.alten.page.SearchWordPage;
import src.laurasalas.alten.utils.Configuration;

/**
 * Unit test for simple App.
 */
public class AppTest {
	
	static Configuration config;
	static WebDriver driver;
	String url = "https://www.google.com/";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {		
		config = new Configuration();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.close();
	}

	@Before
	public void setUp() throws Exception {
		driver = config.initializeTestingEnvironment();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	}

	@After
	public void tearDown() throws Exception {
		config.closeTestingEnvironment();
	}

	@Test
	public void searchWordAlten() {
		
		SearchWordPage searchWordPage = new SearchWordPage(driver);
		searchWordPage.searchWordAlten();
		searchWordPage.resultsSearch();
	}
}
