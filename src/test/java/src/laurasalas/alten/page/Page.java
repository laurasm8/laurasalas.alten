package src.laurasalas.alten.page;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {
	protected WebDriver driver = null;
	
	private By login = By.className("login");
	private By emailSignIn = By.id("email");
	private By passwordSignIn = By.id("passwd");
	private By signIn = By.id("SubmitLogin");
	private By signOut = By.className("logout");

	public Page(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public WebElement waitForElementVisibility(By element) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement foundElement=wait.until(ExpectedConditions.visibilityOfElementLocated(element));
		
		if (foundElement!=null) {
			return foundElement;
		} else {
			throw new Exception("Element "+element.toString()+"is not found");
		}
	} 
	
	public WebElement waitForElementClickable(By element) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 100);
		WebElement foundElement=wait.until(ExpectedConditions.elementToBeClickable(element));
		
		if (foundElement!=null) {
			return foundElement;
		} else {
			throw new Exception("Element "+element.toString()+"is not found");
		}
	} 
	
	public void scrollDown(String scrollNumber) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("scroll(0," + scrollNumber + ");");
		}catch(Exception e) {			
			throw new AssertionError();
		}
	}
	
	public void scrollUp(String scrollNumber) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("scroll(0," + scrollNumber + ");");
		}catch(Exception e) {			
			throw new AssertionError();	
		}
	}
	
	public void login() {
		try {					
			WebElement loginWe = driver.findElement(login);
			loginWe.click();			
		}catch (Exception e) {
			throw new AssertionError();	
		}
	}
	
	public void signIn(String emailStr, String passwordStr) {	
		try {
			WebElement email = driver.findElement(emailSignIn);
			email.sendKeys(emailStr);			
			
			WebElement password = driver.findElement(passwordSignIn);
			password.sendKeys(passwordStr);	
			
			driver.findElement(signIn).click();	
			
		}catch (Exception e) {
			throw new AssertionError();	
		}
	}
	
	public void signOut() {
		try {			
			String signInStr = "Sign in";
			driver.findElement(signOut).click();
			
			WebElement verifySignOut = driver.findElement(login);			
			assertEquals(verifySignOut.getText(), signInStr);
			
		}catch (Exception e) {
			throw new AssertionError();
		}catch (AssertionError e) {
			throw new AssertionError();	
		}
	}
}
