package src.laurasalas.alten.page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import src.laurasalas.alten.factory.SearchWordFactory;

public class SearchWordPage extends Page {

	protected WebDriver driver = null;

	public SearchWordPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	public void searchWordAlten() {		
		WebElement inputGoogle = driver.findElement(By.className(SearchWordFactory.inputGoogleStr));
		
		Actions actions = new Actions(driver);
		actions.moveToElement(inputGoogle);
		actions.click();
		actions.sendKeys(SearchWordFactory.altenWordStr);			
		actions.sendKeys(Keys.ENTER);		
		actions.build().perform();	
	}
	
	public void resultsSearch() {
		List<WebElement> resultsSearchList = driver.findElements(By.className(SearchWordFactory.resultsSearchStr));		
		System.out.println("Alten results: ");
		
		for(WebElement resultsSearch : resultsSearchList) {
			System.out.println(resultsSearch.getText());
		}
		
		System.out.println("Total results in the first page: " + resultsSearchList.size());
	}
}
