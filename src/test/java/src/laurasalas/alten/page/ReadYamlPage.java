package src.laurasalas.alten.page;

import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.yaml.snakeyaml.*;

public class ReadYamlPage {

	public void ReadYamlFile() {
		
		Yaml yaml = new Yaml();
		
		InputStream inputStream = this.getClass()
		  .getClassLoader()
		  .getResourceAsStream("ejemplo.yaml");
		Map<String, Object> obj = yaml.load(inputStream);
		
		System.out.println("Obj: " + obj);
		
		Iterator entries = obj.entrySet().iterator();
				
		while (entries.hasNext()) {
		    Map.Entry entry = (Map.Entry) entries.next();
		    String key = (String)entry.getKey();
		    Set<String> keys = obj.keySet();
		    String[] arr = keys.toArray(new String[0]);
		    //Set<Integer> value = (Set<Integer>)entry.getValue();
			System.out.println("Key = " + key + ", Value = " + arr.toString());
		}
		
	}
}
